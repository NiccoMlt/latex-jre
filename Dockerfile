FROM miktex/miktex

USER root

RUN apt-get update && apt-get -y install \
    openjdk-8-jre-headless \
    python-pygments \
    gnuplot \
    graphviz \
    make \
    git \
 && rm -rf /var/lib/apt/lists/*

RUN mpm --find-updates --admin  
RUN mpm --update --admin
