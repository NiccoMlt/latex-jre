# LaTeX on Docker with JRE #

Docker image with LaTeX distribution and Java 8 support for arara

This was developed to be used by the CI to buid the report for our [SIM-ScALA-BIM 
project](https://gitlab.com/abra-team/sim-scala-bim/).

To use it in GitLab CI, simply add it to [`image: `](https://docs.gitlab.com/ee/ci/yaml/#image-and-services) key in `.gitlab-ci.yml` configuration file:

```YAML
image: niccomlt/latex-jre
```
